using System;
using System.Collections.Generic;
using TwinCAT.Ads;
namespace MinimalExampleAds
{
    public class PublisherManualAdsClient
    {
        public static void Main()
        {
            var client = new PublisherManualAdsMin("5.46.63.220.1.1", 851);
            client.TransmitData("TestByte", 127);
        }
    }
    public class PublisherManualAdsMin
    {
        private readonly TcAdsClient _client;
        private Dictionary<string, int> _knownHandles;
        public PublisherManualAdsMin(string amsNetId, uint port)
        {
            _client = new TcAdsClient();
            _client.Connect(new AmsAddress(amsNetId + ":" + port));
            _knownHandles = new Dictionary<string, int>();
        }
        public void TransmitData<T>(string channel, T data)
        {
            int handle;
            if (_knownHandles.TryGetValue(channel, out var h)) handle = h;
            else
            {
                handle = _client.CreateVariableHandle(channel);
                _knownHandles.Add (channel, handle);
            }
            _client.WriteAny (handle, data);
        }
    }
}