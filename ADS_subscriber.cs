using System;
using System.Collections.Generic;
using System.Text;
using TwinCAT.Ads;
namespace MinimalExampleAds
{
    public class SubscriberManualAdsClient
    {
        public static void Main()
        {
            var client = new SubscriberManualAdsMin("5.46.63.220.1.1", 851);
            client.Subscribe<byte>("TestByte", ReceivedHandler);
            Console.ReadLine();
        }
        public static void ReceivedHandler(string message)
        { 
        }
    }
    public class SubscriberManualAdsMin
    {
        public delegate void ReceivedHandler(string message);
        private readonly TcAdsClient _client;
        private readonly Dictionary<uint, AdsNotificationEventHandler> _subscriptions;
        private readonly Dictionary<string, int> _knownVariableHandles;
        public event ReceivedHandler TestByteReceived;
        public SubscriberManualAdsMin(string amsNetId, uint port)
        {
            _subscriptions = new Dictionary<uint, AdsNotificationEventHandler>();
            _knownVariableHandles = new Dictionary<string, int>();
            _client = new TcAdsClient();
            _client.Connect(new AmsAddress(amsNetId + ":" + port));
            _client.AdsNotification += (s, e) =>
            {
                if (_subscriptions.TryGetValue((uint)e.NotificationHandle, out var handler))
                handler.Invoke(s, e);
            };
        }
        private int GetVariableHandle(string varName)
        {
            if (_knownVariableHandles.TryGetValue(varName, out var handle)) return handle;
            else
            {
                var newHandle = _client.CreateVariableHandle(varName);
                _knownVariableHandles.Add(varName, newHandle);
                return newHandle;
            }
        }
        public void Subscribe<T>(string channel, ReceivedHandler handler)
        {
            var settings = new NotificationSettings(AdsTransMode.OnChange, 10, 20);
            var errorCode = _client.TryAddDeviceNotification(channel, new AdsStream(), 0, 40, settings, null, out uint handle);
            if (errorCode != AdsErrorCode.NoError) throw new Exception("subscription failed with error code" + errorCode);
            _subscriptions.Add(handle, (s, e) =>
            {
                object value;
                if (typeof(T) == typeof(string)) value = _client.ReadAnyString(GetVariableHandle(channel), 80, Encoding.Default);
                else value = _client.ReadAny(GetVariableHandle(channel), typeof(T));
                handler.Invoke(value.ToString());
            });
        }
    }
}