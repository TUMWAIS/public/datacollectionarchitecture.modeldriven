using System;
using System.Text;
using RabbitMQ.Client;
namespace MinimalExample
{
    public class PublisherManualAmqpMinClient
    {
        public static void Main()
        {
            var client = new PublisherManualAmqpMin("192.168.80.214", 5672, "SimplPub", "SimplePass");
            client.TransmitData("TestByte", 127);
        }
    }
    public class PublisherManualAmqpMin
    {
        private IModel _Channel;
        public PublisherManualAmqpMin(string host, uint port,string user, string password)
        {
            var factory =
                new ConnectionFactory {
                    HostName = host,
                    Port = (int) port,
                    UserName = user,
                    Password = password
                };
            _Channel = factory.CreateConnection().CreateModel();
        }
        public void TransmitData(string channel, object data)
        {
            _Channel.QueueDeclare(channel, false, false, false, null);
            _Channel.BasicPublish("", channel, null, Encoding.UTF8.GetBytes(data.ToString()));
        }
    }
}