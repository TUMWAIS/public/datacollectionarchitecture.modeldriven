using System;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
namespace MinimalExample
{
    public class SubscriberManualAmqpMinClient
    {
        public static void Main()
        {
            var client = new SubscriberManualAmqpMin("192.168.80.215", 5672, "SimplePub", "SimplePass");
            client.Subscribe("TestByte", ReceivedHandler);
            Console.ReadLine();
        }
        public static void ReceivedHandler(string message)
        {
        }
    }
    public class SubscriberManualAmqpMin
    {
        private readonly IModel _Channel;
        public delegate void ReceivedHandler(string message);
        public event ReceivedHandler TestByteReceived;
        public SubscriberManualAmqpMin(string host, uint port, string user, string password)
        {
            var factory = new ConnectionFactory
            {
                HostName = host,
                Port = (int)port,
                UserName = user,
                Password = password
            };
            _Channel = factory.CreateConnection().CreateModel();
        }
        public void Subscribe(string channel, ReceivedHandler handler)
        {
            _Channel.QueueDeclare(channel, false, false, false, null);
            _Channel.QueueBind(channel, "", channel);
            var consumer = new EventingBasicConsumer(_Channel);
            consumer.Received += (model, ea) =>
            {
                if (ea.RoutingKey != channel) return;
                var body = ea.Body;
                var message = Encoding.UTF8.GetString(body);
                handler(message);
            };
            _Channel.BasicConsume(channel, true, consumer);
        }
    }
}