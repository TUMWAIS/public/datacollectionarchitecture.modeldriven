using System;
using Confluent.Kafka;
namespace MinimalExample
{
    public class PublisherManualKafkaMinClient
    {
        public static void Main()
        {
            var client = new PublisherManualKafkaMin("192.168.80.214", 1883, "SimplPub", "SimplePass");
            client.TransmitData("TestByte", 127);
        }
    }
    public class PublisherManualKafkaMin
    {
        private IProducer<Ignore, string> _client;
        public PublisherManualKafkaMin( string host, uint port, string user, string password)
        {
            var conf =
                new ProducerConfig {
                    BootstrapServers = host + ":" + port,
                    SaslUsername = user,
                    SaslPassword = password,
                    SecurityProtocol = SecurityProtocol.SaslPlaintext
                };
            _client = new ProducerBuilder<Ignore, string>(conf).Build();
        }
        public void TransmitData(string channel, object data)
        {
            _client.Produce(channel, new Message<Ignore, string> { Value = data.ToString() });
        }
    }
}