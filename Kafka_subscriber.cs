using Confluent.Kafka;
using System;
using System.Collections.Generic;
using System.Threading;
namespace MinimalExample
{
    public class SubscriberManualKafkaMinClient
    {
        public static void Main()
        {
            var client = new SubscriberManualKafkaMin("192.168.80.216", 9092, "SimplePub", "SimplePass");
            client.Subscribe("TestByte", ReceivedHandler);
            Console.ReadLine();
        }
        public static void ReceivedHandler(string message)
        {
        }
    }
    public class SubscriberManualKafkaMin
    {
        public delegate void ReceivedHandler(string message);
        private IConsumer<Ignore, string> _client;
        private Dictionary<string, ReceivedHandler> _channels;
        public event ReceivedHandler TestByteReceived;
        public SubscriberManualKafkaMin(string host, uint port, string user, string password)
        {
            var conf = new ConsumerConfig
            {
                GroupId = "AIS",
                BootstrapServers = host + ":" + port,
                SaslUsername = user,
                SaslPassword = password,
                SecurityProtocol = SecurityProtocol.SaslPlaintext,
            };
            _client = new ConsumerBuilder<Ignore, string>(conf).Build();
            _channels = new Dictionary<string, ReceivedHandler>();
            new Thread(Receive).Start();
        }
        public void Subscribe(string channel, ReceivedHandler handler)
        {
            _client.Subscribe(channel);
            _channels.Add(channel, handler);
        }
        private void Receive()
        {
            while (true)
            {
                try
                {
                    var res = _client.Consume(TimeSpan.FromMilliseconds(100));
                    if (!_channels.TryGetValue(res.Topic, out ReceivedHandler handler)) return;
                    handler.Invoke(res.Message.Value);
                }
                catch(Exception e) { }
            }
        }
    }
}