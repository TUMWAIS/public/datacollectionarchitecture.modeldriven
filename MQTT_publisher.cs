using System;
using OpenNETCF.MQTT;
namespace MinimalExample
{
    public class PublisherManualMqttMinClient
    {
        public static void Main()
        {
            var client = new PublisherManualMqttMin("192.168.80.214", 1883, "SimplPub", "SimplePass");
            client.TransmitData("TestByte", 127);
        }
    }
    public class PublisherManualMqttMin
    {
        private MQTTClient _client;
        public PublisherManualMqttMin( string host, uint port, string user, string password)
        {
            _client = new MQTTClient(host, (int) port);
            _client.Connect("SimplePub", user, password);
        }
        public void TransmitData(string channel, object data)
        {
            _client.Publish(channel, data.ToString(), QoS.FireAndForget, false);
        }
    }
}