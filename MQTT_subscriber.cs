using System;
using OpenNETCF.MQTT;
using System.Collections.Generic;
using System.Text;
namespace MinimalExample
{
    public class SubscriberManualMqttMinClient
    {
        public static void Main()
        {
            var client = new SubscriberManualMqttMin("192.168.80.214", 1883, "SimplPub", "SimplePass");
            client.Subscribe("TestByte", ReceivedHandler);
            Console.ReadLine();
        }
        public static void ReceivedHandler(string message)
        {
        }
    }
    public class SubscriberManualMqttMin
    {
        private readonly MQTTClient _client;
        public delegate void ReceivedHandler(string message);
        private Dictionary<string, ReceivedHandler> _channels;
        public event ReceivedHandler TestByteReceived;
        public SubscriberManualMqttMin(string host, uint port, string user, string password)
        {
            _client = new MQTTClient(host, (int)port);
            _client.Connect("SimplePub", user, password);
            _client.MessageReceived += (channel, qos, payload) =>
            {
                if (!_channels.TryGetValue(channel, out ReceivedHandler handler)) return;
                handler.Invoke(Encoding.UTF8.GetString(payload));
            };
            _channels = new Dictionary<string, ReceivedHandler>();
        }
        public void Subscribe(string channel, ReceivedHandler handler)
        {
            if (!_channels.ContainsKey(channel)) _channels.Add(channel, handler);
        }
    }
}