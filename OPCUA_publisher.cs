using System;
using Opc.Ua.Client;
using Opc.Ua;
namespace MinimalExample
{
    public class PublisherManualOpcUaMinClient
    {
        public static void Main()
        {
            var client = new PublisherManualOpcUaMin("192.168.80.215", 5672);
            client.TransmitData("ns=2;s=TestByte", 127);
        }
    }
    class PublisherManualOpcUaMin
    {
        private Session m_session;
        public PublisherManualOpcUaMin(string host, uint port)
        {
            var opcClientConfig = new ApplicationConfiguration()
            {
                ApplicationName = "OPC UA Data Adapter Pub",
                ApplicationType = ApplicationType.Client,
                ApplicationUri = "urn:" + Utils.GetHostName() + ":AIS:DataAdapterPub",
                SecurityConfiguration = new SecurityConfiguration()
                {
                    ApplicationCertificate = new CertificateIdentifier()
                    {
                        StoreType = CertificateStoreType.Directory,
                        StorePath = "OPC_UA_DataAdapter_Pub\\UA_MachineDefault",
                        SubjectName = "OPA UA Data Adapter",
                    },
                    TrustedPeerCertificates = new CertificateTrustList()
                    {
                        StoreType = CertificateStoreType.Directory,
                        StorePath = "OPC_UA_DataAdapter_Pub\\UA_Applications"
                    }
                },
                ClientConfiguration = new ClientConfiguration()
            };
            opcClientConfig.Validate(ApplicationType.Client).Wait();
            var serverEndpoint = CoreClientUtils.SelectEndpoint("opc.tcp://" + host + ":" + port, false);
            var server = new ConfiguredEndpoint(serverEndpoint.Server, EndpointConfiguration.Create(opcClientConfig));
            server.Update(serverEndpoint);
            m_session = Session.Create(opcClientConfig, server, true, opcClientConfig.ApplicationName, 3600, new UserIdentity(new AnonymousIdentityToken()), null).Result;
        }
        public void TransmitData(string channel, object data)
        {
            WriteValue valueToWrite = new WriteValue
            {
                NodeId = channel,
                AttributeId = Attributes.Value
            };
            valueToWrite.Value.Value = data;
            valueToWrite.Value.SourceTimestamp = DateTime.Now;
            var valuesToWrite = new WriteValueCollection { valueToWrite };
            m_session.Write(null, valuesToWrite, out _, out _);
        }
    }
}