using System;
using Opc.Ua;
using Opc.Ua.Client;
namespace MinimalExample
{
    public class SubscriberManualOpcUaMinClient
        {
        public static void Main()
        {
            var client = new SubscriberManualOpcUaMin("desktop-o6ueut2", 50000);
            client.Subscribe("ns=2;s=TestByte", ReceivedHandler);
            Console.ReadLine();
        }
        public static void ReceivedHandler(string message)
        {
        }
    }
    public class SubscriberManualOpcUaMin
    {
        public delegate void ReceivedHandler(string message);
        private readonly Session m_session;
        public SubscriberManualOpcUaMin(string host, uint port)
        {
            var opcClientConfig = new ApplicationConfiguration()
            {
                ApplicationName = "OPC UA Data Adapter",
                ApplicationType = ApplicationType.Client,
                ApplicationUri = "urn:" + Utils.GetHostName() + ":AIS:DataAdapter",
                SecurityConfiguration = new SecurityConfiguration()
                {
                    ApplicationCertificate = new CertificateIdentifier()
                    {
                        StoreType = CertificateStoreType.Directory,
                        StorePath = "OPC_UA_DataAdapter\\UA_MachineDefault",
                        SubjectName = "OPA UA Data Adapter",
                    },
                    TrustedPeerCertificates = new CertificateTrustList()
                    {
                        StoreType = CertificateStoreType.Directory,
                        StorePath = "OPC_UA_DataAdapter\\UA_Applications"
                    }
                },
                ClientConfiguration = new ClientConfiguration()
            };
            opcClientConfig.Validate(ApplicationType.Client).Wait();
            var serverEndpoint = CoreClientUtils.SelectEndpoint("opc.tcp://" + host + ":" + port, false);
            var serverConfiguration = EndpointConfiguration.Create(opcClientConfig);
            var server = new ConfiguredEndpoint(serverEndpoint.Server, serverConfiguration);
            server.Update(serverEndpoint);
            m_session = Session.Create(opcClientConfig, server, true, opcClientConfig.ApplicationName, 3600, new UserIdentity(new AnonymousIdentityToken()), null).Result;
        }
        public void Subscribe(string channel, ReceivedHandler handler)
        {
            var item = new MonitoredItem()
            {
                DisplayName = channel,
                StartNodeId = channel
            };
            item.Notification += (itm, args) =>
            {
                if (itm.DisplayName == channel)
                    foreach (var val in itm.DequeueValues())
                        handler.Invoke(val?.Value?.ToString());
            };
            var subscription = new Subscription(m_session.DefaultSubscription);
            subscription.AddItem(item);
            m_session.AddSubscription(subscription);
            subscription.Create();
        }
    }
}