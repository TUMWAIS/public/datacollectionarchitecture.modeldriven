# datacollectionarchitecture.modeldriven

Public repository containing the code templates and graphical models for the quantitative evaluation of the model-driven generation of data collection architectures in paper "Model-driven Data Collection Architectures for Cyber-physical Systems of Systems" published in "Sensors" (DOI: <https://doi.org/10.3390/s21030745>). 

__Cite as:__

E. Trunzer, B. Vogel-Heuser, J.-K. Chen, and M. Kohnle, “Model-Driven Approach for Realization of Data Collection Architectures for Cyber-Physical Systems of Systems to Lower Manual Implementation Efforts,” Sensors, vol. 21, no. 3, p. 745, Jan. 2021 [Online]. Available: <http://dx.doi.org/10.3390/s21030745>

## Note

The provided code samples reflect minimal working examples to implement the same communication using the respective protocols. They follow the same coding styles and include sample data, as well as minimal security features (sample passwords, certificates where required) and should not be used as-is in a productive environment.

## Environment

* C# 8.0
* .NET Core 3.1
* Libraries (for protocol support)
  * AMQP: RabbitMQ .Net Client, RabbitMQ.Net client. Version 6.0.0-pre3, <https://github.com/rabbitmq/rabbitmq-dotnet-client>
  * Beckhoff TwinCAT ADS, <https://infosys.beckhoff.com/content/1033/tcadscommon/html/tcadscommon_intro.htm?id=898081192215463875>
  * Kafka: Confluent.Kafka Version, 1.0.0-RC2, <https://github.com/confluentinc/confluent-kafka-dotnet>
  * MQTT: NETCF MQTT, OpenNETCF MQTT. Version 1.0.17253, <https://github.com/ctacke/mqtt>
  * OPC UA Foundation Reference Stack, UA-.NetStandard. Version 1.4.355.26, <https://github.com/OPCFoundation/UA-.NETStandard>
